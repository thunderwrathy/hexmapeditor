using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour
{
    public HexCoordinates coordinates;
    public Color color;
    public RectTransform ui_rect;
    int elevation;
    public int Elevation
    {
        get
        {
            return elevation;
        }
        set
        {
            elevation = value;
            Vector3 position = transform.localPosition;
            position.y = value * HexMetrics.elevation_step;
            transform.localPosition = position;

            Vector3 ui_position = ui_rect.localPosition;
            ui_position.z = elevation * - HexMetrics.elevation_step;
            ui_rect.localPosition = ui_position;
        }
    }

    [SerializeField]
    HexCell[] neighbors;

    public HexCell GetNeighbor(HexDirection direction)
    {
        return neighbors[((int)direction)];
    }

    public void SetNeighbor(HexDirection direction, HexCell cell) 
    {
        neighbors[(int)direction] = cell;
        cell.neighbors[(int)direction.Opposite()] = this;
    }

    public HexEdgeType GetEdgeType(HexDirection direction)
    {
        return HexMetrics.GetHexEdgeType(elevation, neighbors[(int)direction].elevation);
    }

    public HexEdgeType GetEdgeType(HexCell other_cell)
    {
        return HexMetrics.GetHexEdgeType(elevation, other_cell.elevation);
    }
}
