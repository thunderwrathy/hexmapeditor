using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HexMetrics
{
	public const float outerRadius = 10f;
	public const float innerRadius = outerRadius * 0.866025404f;

	public const float blend_factor = 0.20f;
	public const float solid_factor = 1 - blend_factor;

	public const float elevation_step = 5f;

	public const int terraces_per_slope = 2;
	public const int terrace_steps = terraces_per_slope * 2 + 1;
	public const float horizontal_terrace_step_size = 1f / terrace_steps;
	public const float vertical_terrace_step_size = 1f / (terraces_per_slope + 1);

	static Vector3[] corners = {
		new Vector3(0f, 0f, outerRadius),
		new Vector3(innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(0f, 0f, -outerRadius),
		new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
		new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
		new Vector3(0f, 0f, outerRadius)		// First corner duplicated, else it will throw IndexOutOfBoundsException when triangulating the last triangle
	};

	public static Vector3 GetFirstCorner(HexDirection direction)
    {
		return corners[(int)direction];
    }

	public static Vector3 GetSecondCorner(HexDirection direction)
    {
		return corners[(int)direction+1];
	}

	public static Vector3 GetFirstSolidCorner(HexDirection direction)
    {
		return corners[(int)direction] * solid_factor;
	}

	public static Vector3 GetSecondSolidCorner(HexDirection direction)
    {
		return corners[(int)direction+1] * solid_factor;
	}

	public static Vector3 GetBridge(HexDirection direction)
	{
		return (corners[(int)direction] + corners[(int)direction + 1]) *blend_factor;
	}

	public static Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
    {
		float h = step * horizontal_terrace_step_size;
		a.x += (b.x - a.x) * h;
		a.z += (b.z - a.z) * h;
		float v = ((step + 1) / 2) * vertical_terrace_step_size;
		a.y += (b.y - a.y) * v;
		return a;
    }

	public static Color TerraceLerp(Color a, Color b, int step)
    {
		float h = step * horizontal_terrace_step_size;
		return Color.Lerp(a, b, h);
    }

	public static HexEdgeType GetHexEdgeType(int elevation1, int elevation2)
    {
		if (elevation1 == elevation2)
			return HexEdgeType.Flat;

		int delta = elevation2 - elevation1;
		if (delta == 1 || delta == -1)
			return HexEdgeType.Slope;

		return HexEdgeType.Cliff;
    }
}

public enum HexEdgeType
{
	Flat, Slope, Cliff
}