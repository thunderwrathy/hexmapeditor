using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexGrid : MonoBehaviour
{

	public int width = 6;
	public int height = 6;

	public Color defaultColor = Color.white;

	public HexCell cellPrefab;

	HexCell[] cells;

	public Text cellLabelPrefab;
	Canvas gridCanvas;

	HexMesh hexMesh;
	

	void Awake()
	{
		gridCanvas = GetComponentInChildren<Canvas>();
		hexMesh = GetComponentInChildren<HexMesh>();
		cells = new HexCell[height * width];

		for (int z = 0, i = 0; z < height; z++)
		{
			for (int x = 0; x < width; x++)
			{
				CreateCell(x, z, i++);
			}
		}
	}

	void Start()
	{
		hexMesh.Triangulate(cells);
	}

	void CreateCell(int x, int z, int i)
	{
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
		position.y = 0f;
		position.z = z * (HexMetrics.outerRadius * 1.5f);

		HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
		cell.transform.SetParent(transform, false);
		cell.transform.localPosition = position;
		cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);
		cell.color = defaultColor;


		// Setting E-W neighbor cell connections
		if (x > 0)
		{
			cell.SetNeighbor(HexDirection.W, cells[i - 1]);
		}

		
		if (z > 0)
		{
			if ((z & 1) == 0)	// If there is even number of cells in the row
			{
				cell.SetNeighbor(HexDirection.SE, cells[i - width]);			// SE-NW cell connections
				if (x > 0)
				{
					cell.SetNeighbor(HexDirection.SW, cells[i - width - 1]);	// SW-NE cell connections
				}
			}

			else                // If there is odd number of cells in the row
			{
				cell.SetNeighbor(HexDirection.SW, cells[i - width]);			// SW-NE cell connections
				if (x < width - 1)
				{
					cell.SetNeighbor(HexDirection.SE, cells[i - width + 1]);    // SE-NW cell connections
				}
			}
		}

		Text label = Instantiate<Text>(cellLabelPrefab);
		label.rectTransform.SetParent(gridCanvas.transform, false);
		label.rectTransform.anchoredPosition =
			new Vector2(position.x, position.z);
		label.text = cell.coordinates.ToStringOnSeparateLines();
		cell.ui_rect = label.rectTransform;
	}

	public HexCell GetCell(Vector3 position)
	{
		position = transform.InverseTransformPoint(position);
		HexCoordinates coordinates = HexCoordinates.FromPosition(position);		// Touched cell's coordinates

		int index = coordinates.X + coordinates.Z * width + coordinates.Z / 2;	// Array index of touched cell
		HexCell cell = cells[index];
		return cell;
	}

	public void Refresh()
    {
		hexMesh.Triangulate(cells);     // Re-triangulate mesh to load the changes
	}

}