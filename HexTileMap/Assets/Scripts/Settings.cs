using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public float look_sensitivity = 0.5f;
    public float zoom_sensitivity = 0.75f;

    private void Update()
    {
        if(Input.GetKey("up"))
            MoveCameraBy(0f, 1f * look_sensitivity, 0f);

        if (Input.GetKey("down"))
            MoveCameraBy(0f, -1f * look_sensitivity, 0f);

        if (Input.GetKey("right"))
            MoveCameraBy(1f * look_sensitivity, 0f, 0f);

        if (Input.GetKey("left"))
            MoveCameraBy(-1f * look_sensitivity, 0f, 0f);

        if (Input.GetKey("."))
            MoveCameraBy(0f, 0f, 1f * zoom_sensitivity);

        if (Input.GetKey("-"))
            MoveCameraBy(0f, 0f, -1f * zoom_sensitivity);
    }

    public void MoveCameraBy(float x, float y, float z)     // +x: to the right, +y: up, +z: zoom in
    {
        float xAxisValue = Input.GetAxis("Horizontal");
        float zAxisValue = Input.GetAxis("Vertical");
        if (Camera.current != null)
        {
            Camera.current.transform.Translate(new Vector3(xAxisValue+x, y, zAxisValue+z));
        }
    }
}