using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class HexMapEditor : MonoBehaviour
{
	public Color[] colors;
	public HexGrid hex_grid;
	public RawImage background;
	private Color active_color;
	int active_elevation;

	void Awake()
	{
		SelectColor(0);
	}

	void Update()
	{
		if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			HandleInput();
		}
	}

	void HandleInput()
	{
		Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(inputRay, out hit))
		{
			EditCell(hex_grid.GetCell(hit.point));
		}
	}

	public void SelectColor(int index)
	{
		active_color = colors[index];

		//background.color = active_color;


		Texture2D tex2d = new Texture2D(1, 1);
		Color fillColor = active_color;
		Color[] fillColorArray = tex2d.GetPixels();

		for (var i = 0; i < fillColorArray.Length; ++i)
		{
			fillColorArray[i] = fillColor;
		}

		tex2d.SetPixels(fillColorArray);

		tex2d.Apply();
		background.texture = tex2d;
	}


	public void SetElevation(float elevation)
    {
		active_elevation = (int)elevation;
    }

	public void OnSliderValueChanged(float new_value)
	{
		//HexMetrics.SetBlendFactor = new_value;
	}

	public void EditCell(HexCell cell)
    {
		cell.color = active_color;
		cell.Elevation = active_elevation;
		hex_grid.Refresh();
	}
}